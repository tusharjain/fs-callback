/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const problem1 = require('../problem1');

const folderPath = './jsonFolder'

problem1(folderPath); 