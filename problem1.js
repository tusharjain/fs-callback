/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs');

module.exports = problem1; 

function problem1(folderPath) {
    
    fs.access(folderPath, (err) => {
        if(err === null) {
            console.error(`${folderPath} already exists`);
            return; 
        }
        else {
            fs.mkdir(folderPath, {recursive: true}, (err) => {
                if(err) {
                    console.error(err); 
                    return;
                }
                else {
                    for(let fileNo = 0; fileNo < 5; fileNo++) {
                        fs.writeFile(`${folderPath}/testFile${fileNo}.json`, JSON.stringify({a: 1, b: 2}, null, 2), (err) => {
                            if(err) {
                                console.error(err);
                                return;
                            }
                            else {
                                console.log(`Created testFile${fileNo}`);
                                setTimeout(() => {
                                    fs.unlink(`${folderPath}/testFile${fileNo}.json`, (err) => {
                                        if(err) {
                                            console.error(err); 
                                            return; 
                                        }
                                        else {
                                            console.log(`Deleted testFile${fileNo}`);
                                        }
                                    });
                                }, 2 * 1000);
                            }
                        });
                    }
                }
            });
        }
    });
}

