/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');

module.exports = problem2;

function problem2(testFile) {
    fs.readFile(testFile, 'utf-8', (err, data) => {
        if(err) {
            console.error(err); 
        }
        else {
            data = data.toUpperCase(); 
            fs.writeFile('uppercase.txt', data, (err) => {
                if(err) {
                    console.error(err); 
                    return; 
                }
                else {
                    console.log('Created "uppercase.txt"');
                    fs.writeFile('filenames.txt', 'uppercase.txt\n',(err) => { // write to filenames.txt
                        if(err) {
                            console.error(err);
                            return;
                        }
                        else {
                            console.log('Created "filenames.txt"');
                            // no subsequent process
                        }
                    });

                    fs.readFile('uppercase.txt', 'utf-8', (err, data) => {
                        if(err) {
                            console.error(err); 
                            return; 
                        }
                        else {
                            data = data.toLowerCase()
                                       .split(/(?<=\.)\s(?=\w)/);
                            fs.writeFile('lowercaseSplit.txt', JSON.stringify(data, null, 2), (err) => {
                                if(err) {
                                    console.error(err); 
                                    return;
                                }
                                else {
                                    console.log('Created "lowercaseSplit.txt"');
                                    fs.appendFile('filenames.txt', 'lowercaseSplit.txt\n', (err) => { // write to filenames.txt
                                        if(err) {
                                            console.error(err); 
                                            return;
                                        }
                                        else {
                                            console.log('Updated "filenames.txt"');
                                            // no subsequent process
                                        }
                                    });

                                    fs.readFile('lowercaseSplit.txt', 'utf-8', (err, data) => {
                                        if(err) {
                                            console.error(err); 
                                        }
                                        else {
                                            data = JSON.parse(data)
                                                       .sort(); 
                                            fs.writeFile('lowercaseSplitSort.txt', JSON.stringify(data, null, 2), (err) => {
                                                if(err) {
                                                    console.error(err); 
                                                }
                                                else {
                                                    console.log('Created "lowercaseSplitSort.txt"');
                                                    fs.appendFile('filenames.txt', 'lowercaseSplitSort.txt\n', (err) => { // write to filenames.txt
                                                        if(err) {
                                                            console.error(err); 
                                                            return;
                                                        }
                                                        else {
                                                            console.log('Updated "filenames.txt"');
                                                            fs.readFile('filenames.txt', 'utf-8', (err, data) => {
                                                                if(err) {
                                                                    console.error(err);
                                                                    return;
                                                                }
                                                                else {
                                                                    data = data.trim()
                                                                               .split(/\s/)
                                                                               .forEach((file) => {
                                                                                   setTimeout(() => {
                                                                                        fs.unlink(file, (err) => {
                                                                                            if(err) {
                                                                                                console.error(err); 
                                                                                                return; 
                                                                                            }
                                                                                            else {
                                                                                                console.log(`Deleted "${file}"`);
                                                                                            }
                                                                                        });
                                                                                   }, 5 * 1000);
                                                                               });
                                                                }
                                                            });
                                                        }
                                                    });             
                                                }
                                            });
                                        }
                                    });
                                }
                            }); 
                        }
                    });
                }
            });
        }
    });
}

